#include<stdio.h>
#include<stdlib.h>
void sort(int *arr,int size)
{
	int i,j;
	for(i=0;i<size-1;i++)
	{
		for(j=i+1;j<size;j++)
		{
			if(arr[i]>arr[j])
			{
				int temp=arr[i];
				arr[i]=arr[j];
				arr[j]=temp;
			}
		}
	}
}
int main()
{
	int size,*arr,*arr1;
	scanf("%d",&size);
	arr=malloc(size*sizeof(int));
	arr1=malloc(size*sizeof(int));
	int j;
	for(j=0;j<size;j++)
	{
		scanf("%d",&arr[j]);
		arr1[j]=arr[j];
	}
	sort(arr,size);
	int counter=0;
	for(j=0;j<size;j++)
	{
		if(arr1[j]!=arr[j])
		{
			counter++;
		}
	}
	if(counter<=2)
	{
		printf("YES\n");
	}
	else
	{
		printf("NO\n");
	}
	return 0;

}
