#include<iostream>
using namespace std;
#include<map>
#include<vector>
#include<string>
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        string s1,s2;
        cin>>s1>>s2;
        map<int,vector<int> > count;
        vector<int> v;
        unsigned int i;
        for(i=0;i<s1.size();i++)
        {
                if(s2[i]>=s1[i])
                {
                    if(count[s2[i]-s1[i]].empty())
                    {
                        v.push_back(s2[i]-s1[i]);
                        count[s2[i]-s1[i]].push_back(1);
                    }
                    else
                    {
                        count[s2[i]-s1[i]][0]++;
                    }
                }
                else
                {
                    int num=('z'-s2[i])+(s1[i]-'a'+1);
                    if(count[num].empty())
                    {
                        v.push_back(num);
                        count[num].push_back(1);
                    }
                    else
                    {
                        count[num][0]++;
                    }
                }
        }
        int max=0;
        for(i=0;i<v.size();i++)
        {
            if(count[v[i]][0]>max)
                max=count[v[i]][0];
        }
        cout<<s1.size()-max<<endl;


    }
    return 0;
    }

