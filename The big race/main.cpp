#include<iostream>
using namespace std;
int     gcd( unsigned long long int a, unsigned long long int b ) {
    unsigned long long int     t;

    if ( a < 0 )
        a = -a;

    if ( ! b )
        return a;

    if ( b < 0 )
        b = -b;

    if ( ! a )
        return b;

    t = 0;

    while ( ! ( ( a | b ) & 1 ) ) {
        a >>= 1;
        b >>= 1;
        ++t;
    }

    while ( ! ( a & 1 ) )
        a >>= 1;

    while ( ! ( b & 1 ) )
        b >>= 1;

    while ( a != b ) {
        if ( a > b ) {
            a -= b;

            do {
                a >>= 1;
            } while ( ! ( a & 1 ) );
        }

        else {
            b -= a;

            do {
                b >>= 1;
            } while ( ! ( b & 1 ) );
        }
    }

    return a << t;
}
int main()
{
    unsigned long long int t,w,b;
    cin>>t>>w>>b;
    unsigned long long int i;
    unsigned long long int counter=0;
    for(i=1;i<=t;i++)
    {
        if(i%w==i%b)
        {
            counter++;
        }
    }
    i=2;
    int n=gcd(counter,t);
    counter=counter/n;
    t=t/n;
    cout<<counter<<"/"<<t<<endl;
    return 0;

}
