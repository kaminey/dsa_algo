from math import sqrt; from itertools import count, islice

def isPrime(n):
    if n < 2: return False
    return all(n%i for i in islice(count(2), int(sqrt(n)-1)))

t=int(raw_input())
for i in range(t):
	n=int(raw_input())
	if(n==2):
		print 1000000/2
	else:
		j=n
		counter=0
		while(j<=1000000):
			if(isPrime(j)):
				counter+=1
			j=j+2
		print counter
