big=0
def smallest(h,low,high):
	l=h[low]
	j=low
	for i in range(low,high+1):
		if(h[i]<l):
			l=h[i]
			j=i
	return j
def find(h,low,high):
	global big
	if(high>=low):
		i=smallest(h,low,high)
		value=h[i]*(high-low+1)
		if(value>big):
			big=value
		find(h,i+1,high)
		find(h,low,i-1)

n=int(input())
h=input().split()
for i in range(n):
	h[i]=int(h[i])
find(h,0,n-1)
print(big)
