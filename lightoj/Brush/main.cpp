#include<iostream>
using namespace std;
#include<vector>
#include<algorithm>
int main()
{
    int t;
    cin>>t;
    int i;
    for(i=0;i<t;i++)
    {
        int n,w;
        cin>>n>>w;
        int j;
        vector<int> y;
        for(j=0;j<n;j++)
        {
            int n3;
            int ver;
            cin>>n3>>ver;
            y.push_back(ver);
        }
        sort(y.begin(),y.end());
        int counter=1;
        int start=y[0]+w;
        for(j=0;j<n;j++)
        {
            if(y[j]>start)
            {
                counter++;
                start=y[j]+w;
            }
        }
        cout<<"Case "<<i+1<<": "<<counter<<endl;
    }
    return 0;
}
