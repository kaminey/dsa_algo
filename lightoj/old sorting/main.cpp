#include<iostream>
using namespace std;
#include<vector>
#include<algorithm>
int main()
{
    int t;
    cin>>t;
    int i;
    for(i=0;i<t;i++)
    {
        vector<int> v,v1;
        int n;
        cin>>n;
        int j;
        for(j=0;j<n;j++)
        {
            int num;
            cin>>num;
            v.push_back(num);
            v1.push_back(num);
        }
        sort(v.begin(),v.end());
        int counter=0;
        j=0;
        while(j!=n)
        {
            if(v1[j]==j+1)
            {
                j++;
            }
            else
            {
                int temp=v1[v1[j]-1];
                v1[v1[j]-1]=v1[j];
                v1[j]=temp;
                counter++;
            }
        }

        cout<<"Case "<<i+1<<": "<<counter<<endl;
    }
    return 0;
}
