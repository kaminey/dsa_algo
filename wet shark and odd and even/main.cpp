#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
int main()
{
    int n;
    vector<int> v;
    cin>>n;
    int i;
    unsigned long long int sum=0;
    for(i=0;i<n;i++)
    {
        int num;
        cin>>num;
        if(num%2!=0)
        {
            v.push_back(num);
        }
        sum=sum+num;
    }
    if(sum%2==0)
    {
        cout<<sum<<endl;
    }
    else
    {
        sort(v.begin(),v.end());
        i=0;
        while(sum%2!=0 && i<n)
        {
            sum=sum-v[i];
            i++;
        }
        if(i<n)
        {
            cout<<sum<<endl;
        }
        else
        {
            cout<<0<<endl;
        }
    }
    return 0;
}
