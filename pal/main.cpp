#include<iostream>
using namespace std;
#include<cmath>
#include<vector>
#include<algorithm>
int main()
{
    int t,i;
    cin>>t;
    for(i=0;i<t;i++)
    {
        int num;
        cin>>num;
        vector<int> v;
        while(num!=0)
        {
            int a=num%10;
            v.push_back(a);
            num=num/10;
        }
        unsigned int size=v.size();
        unsigned int mid;
        unsigned int k,j;
        int previous=0;
        if(size%2==0)
        {
            mid=(size-1)/2;
            k=mid;
            j=mid+1;
        }
        else
        {
            k=j=size/2;
        }
        while(k>=0 && j<size)
        {
                if(k==j && v[k]<9)
                {
                    v[k]++;
                    previous=1;
                }
                else
                {
                    if(v[k]>v[j] && previous==0)
                    {
                        v[j]=v[j]+1;
                        v[k]=v[j];
                        previous=1;
                    }
                    else if(v[k]>v[j] && previous==1)
                    {
                        v[k]=v[j];
                    }
                    else if(v[k]<v[j])
                    {
                        v[k]=v[j];
                        previous=1;
                    }
                }
                k--;
                j++;
        }
        int sum=0;
        k=0;
        while(k<size)
        {
            sum=sum+v[k]*pow(10,k);
            k+=1;
        }
        cout<<sum<<endl;
    }
    return 0;
}
