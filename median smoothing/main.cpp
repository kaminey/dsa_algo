#include<iostream>
using namespace std;
#include<vector>
#include<algorithm>
int median(int a,int b,int c)
{
    vector<int> v;
    v.push_back(a);
    v.push_back(b);
    v.push_back(c);
    sort(v.begin(),v.end());
    return v[1];
}
int main()
{
    vector<int> v;
    int n;
    cin>>n;
    int i;
    for(i=0;i<n;i++)
    {
        int num;
        cin>>num;
        v.push_back(num);
    }
    vector<int> m(n);
    int counter=-1;
    do
    {
        if(counter!=-1)
        {
            v=m;
        }
        m[0]=v[0];
        m[n-1]=v[n-1];
        for(i=1;i<n-1;i++)
        {
            if(v[i]>=v[i-1] && v[i+1]>=v[i])
            {
                m[i]=v[i];
            }
            else
            {
                int num=median(v[i-1],v[i],v[i+1]);
                m[i]=num;
            }
        }
        counter++;
        if(counter>n)
        {
            break;
        }
    }while(v!=m);
    if(counter>n)
    {
        cout<<-1;
    }
    else
    {
        cout<<counter<<endl;
        for(i=0;i<n;i++)
        {
            cout<<m[i]<<" ";
        }
    }
    return 0;
}
