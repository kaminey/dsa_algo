def biggest(v):
	i=0
	j=len(v)-1
	if(j>i):
		while(j>i and v[i]==v[j]):
			i+=1
			j-=1
	if(v[i]>v[j]):
		num=v[0]
		v.pop(0)
		return (num,v)
	else:
		num=v[len(v)-1]
		v.pop()
		return (num,v)
t=int(raw_input())
for i in range(t):
	n=int(raw_input())
	v=raw_input().split()
	for j in range(n):
		v[j]=int(v[j])
	a=[]
	while(v):
		num,v=biggest(v)
		counter1=0
		counter2=0
		k=0
		while(k<len(a)):
			if(num>a[k]):
				counter1+=1
			k+=1
		k=len(a)-1
		while(k>=0):
			if(num<a[k]):
				counter2+=1
			k-=1
		if(counter1>counter2):
			a.append(num)
		else:
			a.insert(0,num)
	k=0
	counter=0
	while(k<n-1):
		l=k+1
		while(l<n-1):
			if(a[k]>a[l]):
				counter+=1
			l+=1
		k+=1
	print counter
				
