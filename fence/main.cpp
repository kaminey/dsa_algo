#include<iostream>
using namespace std;
#include<vector>
#include<map>
int dp[10000][10000];
int min_k(vector<int> v,int n,int i,map<int,int> &p,int j,int k)
{
    if(j<k)
    {
        if(i>=n)
            return 0;
        else
            if(dp[i+1][j]!=-1)
                dp[i+1][j]=min_k(v,n,i+1,p,j,k);
            int value1=dp[i+1][j];
            if(dp[i+1][j+1]==-1)
                dp[i+1][j+1]=min_k(v,n,i+1,p,j+1,k);
            int value2=dp[i+1][j+1];
            int value=min(value1,value2+v[i]);
            if(value1==value)
            {
                p[value]=i+1;
                return value;
            }
            else
            {
                p[value]=i;
                return value;
            }
    }
    else
    {
        return 0;
    }
}
int main()
{
    int n,k;
    cin>>n>>k;
    vector<int> v;
    int i;
    int j;
    for(i=0;i<n;i++)
    {
        for(j=0;j<k;j++)
        {
            dp[i][j]=-1;
        }
    }
    for(i=0;i<n;i++)
    {
        int num;
        cin>>num;
        v.push_back(num);
    }
    map<int,int> p;
    int min=min_k(v,n,0,p,0,k);
    cout<<min<<endl;
    return 0;
}

