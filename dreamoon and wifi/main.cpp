#include<iostream>
using namespace std;
#include<string>
#include<iomanip>
float counter;
void see(string instr,int distance,int i,unsigned int n,int final_distance)
{
    if(i!=n+1)
    {
        if(instr[i]=='+')
        {
            i++;
            distance++;
            see(instr,distance,i,n,final_distance);
        }
        else if(instr[i]=='-')
        {
            i++;
            distance--;
            see(instr,distance,i,n,final_distance);
        }
        else
        {
            distance++;
            i++;
            see(instr,distance,i,n,final_distance);
            distance-=2;
            see(instr,distance,i,n,final_distance);
        }
    }
    else
    {
        if(distance==final_distance)
            counter++;
    }
}
int main()
{
    string instr1,instr2;
    counter=0;
    cin>>instr1>>instr2;
    int final_distance=0;
    float total=1.0;
    unsigned int i;
    for(i=0;i<instr1.size();i++)
    {
        if(instr1[i]=='+')
        {
            final_distance++;
        }
        if(instr1[i]=='-')
        {
            final_distance--;
        }
        if(instr2[i]=='?')
        {
            total=total*2;
        }
    }
    int distance=0;
    see(instr2,distance,0,instr1.size()-1,final_distance);
    cout<<setprecision(11)<<counter/total<<endl;
    return 0;
}
