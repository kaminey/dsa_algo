segment={}
def sum_binary(binary,lower,high):
	sum=0
	h=high
	counter=0
	while(h>=lower):
		sum=sum+binary[h]*(2**counter)
		counter+=1
		h+=-1
	return sum
def build(index,binary,lower,high):
	global segment
	if(high>lower):
		mid=(lower+high)//2
		segment[index]=sum_binary(binary,lower,high)
		build(2*index+1,binary,lower,mid)
		build(2*index+2,binary,mid+1,high)
	if(high==lower):
		segment[index]=binary[high]
n=int(raw_input())
bin=raw_input()
binary=[]
for i in range(n):
	num=int(bin[i])
	binary.append(num)
#build(0,binary,0,n-1)
q=int(input())
for i in range(q):
	qy=raw_input().split()
	for j in range(len(qy)):
		qy[j]=int(qy[j])
	if(len(qy)==3):
		l=int(qy[1])
		r=int(qy[2])
		ans=sum_binary(binary,l,r)
		print(ans%3)
	elif(len(qy)==2):
		index=int(qy[1])
		binary[index]=1

