#include<iostream>
using namespace std;
#include<vector>
int n;
int cache[100][100];
int find_max_profit(vector<int> &v,int l,int r)
{
    if(l>r)
    {
        return 0;
    }
    else
    {
        int year=l+n-r;
        if(cache[l][r]!=-1)
        {
            return cache[l][r];
        }
        else
        {

        cache[l][r]=max(find_max_profit(v,l+1,r)+year*v[l],find_max_profit(v,l,r-1)+year*v[r]);
        return cache[l][r];

        }
    }
}
int main()
{
    cout<<"enter the number of wines"<<endl;
    cin>>n;
    int i,j;
    cout<<"enter the prices"<<endl;
    vector<int> v;
    for(i=0;i<n;i++)
    {
        for(j=0;j<n;j++)
        {
            cache[i][j]=-1;
        }
    }
    for(i=0;i<n;i++)
    {
        int num;
        cin>>num;
        v.push_back(num);
    }
    int max=find_max_profit(v,0,n-1);
    cout<<"maximum profit is "<<max<<endl;
    return 0;
}
