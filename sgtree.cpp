#include<iostream>
using namespace std;
#include<vector>
#include<map>
int max(int num1,int num2)
{
	return ((num1>num2)?num1:num2);
}
void build(map< int, int > &segtree,vector<int> &v,int current,int l,int h)
{
	if(l==h)
	{
		segtree[current]=v[l];
	}
	if(l>h)
	{
		int mid=(h+l)/2;
		build(segtree,v,2*current+1,l,mid);
		build(segtree,v,2*current+2,mid+1,h);
		segtree[current]=max(segtree[2*current+1],segtree[2*current+2]);
	}
}
int main()
{
	int n;
	cout<<"enter the size of the array\n"<<endl;
	cin>>n;
	int j;
	vector<int> v;
	for(j=0;j<n;j++)
	{
		int num;
		cin>>num;
		v.push_back(num);
	}
	map< int ,int > segtree;
	build(segtree,v,0,0,n-1);
	return 0;
}
