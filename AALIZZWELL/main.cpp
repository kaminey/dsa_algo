#include<iostream>
using namespace std;
#include<vector>
char str[]="ALLIZZWELL";
void dfs(vector<vector<char > > &v,int pos_y,int pos_x,vector<vector<int> > &reached,int index,int &len,int y,int x)
{
    while(1)
    {
    if(pos_y>=0 && pos_y<y && pos_x>=0 && pos_x<x &&index<10 && reached[pos_y][pos_x]==0 &&str[index]==v[pos_y][pos_x])
    {
            len++;
            index++;
            reached[pos_y][pos_x]=1;
            vector<vector<int> > reached1=reached;
            int t,t1;
            t=len;
            t1=len;
            dfs(v,pos_y-1,pos_x-1,reached,index,t1,y,x);
            reached=reached1;
            if(t1==10)
            {
              len=t1;
              break;
            }
            t1=len;
            dfs(v,pos_y-1,pos_x,reached,index,t1,y,x);
            reached=reached1;
            if(t1==10)
            {
              len=t1;
              break;
            }
            t1=len;
            dfs(v,pos_y-1,pos_x+1,reached,index,t1,y,x);
            reached=reached1;
            if(t1==10)
            {
              len=t1;
              break;
            }
            t1=len;
            dfs(v,pos_y,pos_x+1,reached,index,t1,y,x);
            reached=reached1;
            if(t1==10)
            {
              len=t1;
              break;
            }
            t1=len;
            dfs(v,pos_y+1,pos_x+1,reached,index,t1,y,x);
            reached=reached1;
            if(t1==10)
            {
              len=t1;
              break;
            }
            t1=len;
            dfs(v,pos_y+1,pos_x,reached,index,t1,y,x);
            reached=reached1;
            if(t1==10)
            {
              len=t1;
              break;
            }
            t1=len;
            dfs(v,pos_y+1,pos_x-1,reached,index,t1,y,x);
            reached=reached1;
            if(t1==10)
            {
              len=t1;
              break;
            }
            t1=len;
            dfs(v,pos_y,pos_x-1,reached,index,t1,y,x);
            if(t1==10)
            {
              len=t1;
              break;
            }
            break;
    }
    else
    {
        break;
    }
    }
}
int control_dfs(vector<vector<char> > &v,int start_y,int start_x,int y,int x)
{
            int index=0;
            vector<vector<int> > reached(y,vector<int>(x));
            int i,j;
            for(i=0;i<y;i++)
            {
                for(j=0;j<x;j++)
                {
                    reached[i][j]=0;

                }
            }
            reached[start_y][start_x]=1;
            vector<vector<int> > reached1=reached;
            int len=1;
            dfs(v,start_y-1,start_x-1,reached,index+1,len,y,x);
            if(len==10)
                return 1;
            len=1;
            reached=reached1;
            dfs(v,start_y-1,start_x,reached,index+1,len,y,x);
            if(len==10)
                return 1;
            len=1;
            reached=reached1;
            dfs(v,start_y-1,start_x+1,reached,index+1,len,y,x);
            if(len==10)
                return 1;
            len=1;
            reached=reached1;
            dfs(v,start_y,start_x+1,reached,index+1,len,y,x);
            if(len==10)
                return 1;
            len=1;
            reached=reached1;
            dfs(v,start_y+1,start_x+1,reached,index+1,len,y,x);
            if(len==10)
                return 1;
            len=1;
            reached=reached1;
            dfs(v,start_y+1,start_x,reached,index+1,len,y,x);
            if(len==10)
                return 1;
            len=1;
            reached=reached1;
            dfs(v,start_y+1,start_x-1,reached,index+1,len,y,x);
            if(len==10)
                return 1;
            len=1;
            reached=reached1;
            dfs(v,start_y,start_x-1,reached,index+1,len,y,x);
            if(len==10)
                return 1;
            return 0;


}
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
    int y,x;
    cin>>y>>x;
    vector<vector<char> > v(y,vector<char>(x));
    vector<int> start_x;
    vector<int> start_y;
    int i,j;
    for(i=0;i<y;i++)
    {
        for(j=0;j<x;j++)
        {
            cin>>v[i][j];
            if(v[i][j]=='A')
            {
                start_y.push_back(i);
                start_x.push_back(j);
            }

        }
    }
    if(start_y.size()!=0)
    {
        int n=0;
        int i;
        for(i=0;i<start_y.size();i++)
        {
            if(control_dfs(v,start_y[i],start_x[i],y,x)==1)
            {
                n=1;
                cout<<"YES"<<endl;
                break;
            }
        }
        if(!n)
        {
            cout<<"NO"<<endl;
        }

    }
    else
    {
     cout<<"NO"<<endl;
    }
    }
    return 0;
}
