#include<iostream>
using namespace std;
int main()
{
    int t;
    cin>>t;
    int i;
    for(i=0;i<t;i++)
    {
        unsigned long long int n;
        cin>>n;
        if(n==1)
            cout<<-1<<endl;
        else
        {
            long long int sum=-1;
            long long int k=2;
            while(k<=n)
            {
                    if((k^(k-1))>=k)
                    {
                        sum=sum-k;
                        long long int l=k;
                        k=k<<1;
                        if(k<=n)
                        {
                            sum=sum+(k-l-1)*(l+k)/2;
                        }
                    }
            }
            k=k>>1;
            if(k!=n)
            {
                sum=sum+(n-k)*(n+k+1)/2;
            }
            cout<<sum<<endl;
        }
    }
    return 0;
}
