#include<iostream>
using namespace std;
#include<vector>
#include<map>
int max(int num1,int num2)
{
	return ((num1>num2)?num1:num2);
}
void build(map< int, int > &segtree,vector<int> &v,int current,int l,int h)
{
	if(l==h)
	{
		segtree[current]=v[l];
	}
	else
	{
		int mid=(h+l)/2;
		build(segtree,v,2*current+1,l,mid);
		build(segtree,v,2*current+2,mid+1,h);
		segtree[current]=max(segtree[2*current+1],segtree[2*current+2]);
	}
}
void update(map< int, int > &segtree,vector<int> &v,int current,int index,int l,int h,int num)
{
    if(l==h)
    {
        segtree[current]=num;
        v[l]=num;
    }
    else
    {
        int mid=(l+h)/2;
        if(index>mid)
        {
            update(segtree,v,2*current+2,index,mid+1,h,num);
        }
        else
        {
            update(segtree,v,2*current+1,index,l,mid,num);
        }
        segtree[current]=max(segtree[2*current+1],segtree[2*current+2]);
    }
}
int main()
{
	int n;
	cout<<"enter the size of the array\n"<<endl;
	cin>>n;
	int j;
	vector<int> v;
	for(j=0;j<n;j++)
	{
		int num;
		cin>>num;
		v.push_back(num);
	}
	map< int ,int > segtree;
	build(segtree,v,0,0,n-1);
	int index,num;
    cin>>index>>num;
    cout<<segtree[3]<<endl;
    update(segtree,v,0,index,0,n-1,num);
    cout<<segtree[0]<<endl;
	return 0;
}
