#include<iostream>
using namespace std;
#include<map>
#include<vector>
void dfs(map<int,vector<int> > &tree,int root,int counter,int &max)
{
    if(tree[root].empty())
    {
        if(max<=counter)
        {
            max=counter;
        }
    }
    else
    {
        unsigned int j;
        for(j=0;j<tree[root].size();j++)
        {
            counter++;
            dfs(tree,tree[root][j],counter,max);
            counter--;
        }
    }

}
int main()
{
    map<int,vector<int> > tree;
    int n,root;
    cin>>n;
    int i;
    for(i=1;i<n;i++)
    {
        int num1,num2;
        cin>>num1>>num2;
        if(i==1)
        {
            root=num1;
        }
        tree[num1].push_back(num2);
    }
    int counter=0;
    int max=0;
    dfs(tree,root,counter,max);
    cout<<max<<endl;
    return 0;
}
