#include<iostream>
using namespace std;
#include<vector>
void dfs(vector<vector<char> > &v,int i,int j,vector<vector<int> > &reached,int h,int w,int &total)
{
    if(i>=0 && i<w && j>=0 && j<h)
    {
        if(reached[j][i]==0 && v[j][i]!='#')
        {
            total++;
            reached[j][i]=1;
            dfs(v,i-1,j,reached,h,w,total);
            dfs(v,i+1,j,reached,h,w,total);
            dfs(v,i,j-1,reached,h,w,total);
            dfs(v,i,j+1,reached,h,w,total);
        }
    }

}
int main()
{
    int t;
    cin>>t;
    int k;
    for(k=0;k<t;k++)
    {
        int w,h;
        cin>>w>>h;
        vector<vector<char> > v(h,vector<char>(w));
        int i,j,start_x,start_y;
        vector<vector<int> > reached(h,vector<int>(w));
        for(i=0;i<h;i++)
        {
            for(j=0;j<w;j++)
            {
                char c;
                cin>>c;
                v[i][j]=c;
                reached[i][j]=0;
                if(v[i][j]=='@')
                {
                    start_x=j;
                    start_y=i;
                }
            }
        }
        int total=0;
        dfs(v,start_x,start_y,reached,h,w,total);
        cout<<"Case "<<k+1<<":"<<" "<<total<<endl;
    }
    return 0;
}
