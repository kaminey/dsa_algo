#include<iostream>
using namespace std;
#include<cmath>
int main()
{
	int t;
	cin>>t;
	int i;
	for(i=0;i<t;i++)
	{
		unsigned long long int n;
		cin>>n;
		int turn=1;
		while(n!=1)
		{
			if(turn==1)
				turn=0;
			else
				turn=1;
			unsigned long long int j=1;
			while(pow(2,j)<n)
			{
				j++;
			}
			j--;
			if(pow(2,j)==n)
			{
				n=n/2;
			}
			else
			{
				n=n-pow(2,j);
			}
		}
		if(turn==0)
			cout<<"Louise"<<endl;
		else
			cout<<"Richard"<<endl;

	}
	return 0;
}
