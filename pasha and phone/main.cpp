#include<iostream>
using namespace std;
#include<vector>
int contains(unsigned long long int num,unsigned long long int a)
{
    while(num!=0)
    {
        if(num==a)
            return 1;
        num=num/10;
    }
    return 0;
}
unsigned long long int max_limit(int k)
{
    int i;
    unsigned long long int limit=0;
    for(i=0;i<k;i++)
    {
        limit=limit*10+9;
    }
    return limit;
}
int main()
{
    int n,k;
    cin>>n>>k;
    vector<unsigned long long int> v1;
    vector<unsigned long long int> v2;
    int i;
    for(i=0;i<n/k;i++)
    {
        unsigned long long int num;
        cin>>num;
        v1.push_back(num);
    }
    for(i=0;i<n/k;i++)
    {
        unsigned long long int num;
        cin>>num;
        v2.push_back(num);
    }
    vector<unsigned long long int> counter(n/k);
    for(i=0;i<n/k;i++)
    {
        if(contains(0,v2[i]))
        {
            counter[i]=0;
        }
        else
        {
            counter[i]=1;
        }
    }
    unsigned long long int limit =max_limit(k);
    for(i=0;i<n/k;i++)
    {
        unsigned long long int start=v1[i];
        int j=2;
        while(start<=limit)
        {
            if(!contains(start,v2[i]))
            {
                counter[i]++;
            }
            start=start*j;
        }
    }
    unsigned long long int answer=1;
    for(i=0;i<n/k;i++)
    {
        answer=answer*counter[i];
    }
    cout<<answer<<endl;
    return 0;
}
