#include<stdio.h>
#include<stdlib.h>
int split(int *arr,int l,int h)
{
	int i=l,key;
	key=arr[i];
	int j=h;
	while(i<j)
	{
		while(arr[i]<key && i<h)
		{
			i++;
		}
		while(arr[j]>key)
		{
			j--;
		}
		if(i<j)
		{
			int temp;
			temp=arr[i];
			arr[i]=arr[j];
			arr[j]=temp;
		}
	}
	int temp;
	temp=arr[j];
	arr[j]=arr[i];
	arr[i]=temp;
	return j;
}
int find_k_smallest(int *arr,int l,int h,int k)
{
	if(h>l)
	{
		int j=split(arr,l,h);
		if(k<j)
		{
			return find_k_smallest(arr,l,j-1,k);
		}
		else if(k==j)
		{
			return j;
		}
		else
		{
			return find_k_smallest(arr,j+1,h,k-j-1);
		}
	}
	else if(h==l)
	{
		return arr[l];
	}
	else
	{
		return -1;
	}
		
}
int main()
{
	int n;
	printf("enter the size of the array\n");
	scanf("%d",&n);
	int *arr;
	arr=malloc(n*sizeof(int));
	int i;
	printf("enter the elements\n");
	for(i=0;i<n;i++)
	{
		scanf("%d",&arr[i]);
	}
	int k;
	printf("enter the order statistics of the number you want to find\n");
	scanf("%d",&k);
	i=find_k_smallest(arr,0,n-1,k-1);
	if(i==-1)
	{
		printf("element is not in list\n");
	}
	else
	{
		printf("Number is:%d\n",arr[i]);
	}
	return 0;
}
		
