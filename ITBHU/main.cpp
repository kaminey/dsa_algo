#include<iostream>
using namespace std;
class Counter
{
      private:
    {
        int count;
    }

    public:
    {
        Counter()
        {
            count=0;
        }
        int get_count()
        {
            return count;
        }
        void incr_count(int num)
        {
            count+=num;
        }
    }

};
int main()
{
    Counter j;
    j.incr_count(12);
    cout<<j.get_count()<<endl;
    return 0;
}
