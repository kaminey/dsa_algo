#include<stdio.h>
#include<stdlib.h>
int big;
int min(int *arr,int low,int high)
{
	int i,j;
	int min=arr[low];
	for(i=low;i<=high;i++)
	{
		if(arr[i]<min)
		{
			min=arr[i];
			j=i;
		}
	}
	return j;
}
void find(int *arr,int low,int high)
{
	if(high>low)
	{
		int i=min(arr,low,high);
		int value=arr[i]*(high-low+1);
		if(value>big)
		{
			big=value;
		}
		find(arr,i+1,high);
		find(arr,low,i-1);
	}
}
			
int main()
{
	int n;
	scanf("%d",&n);
	int *arr;
	arr=malloc(n*sizeof(int));
	int i;
	for(i=0;i<n;i++)
	{
		scanf("%d",&arr[i]);
	}
	big=arr[0];
	find(arr,0,n-1);
	printf("%d",big);
	return 0;
}
