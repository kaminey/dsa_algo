#include<iostream>
using namespace std;
#include<vector>
#include<map>
int main()
{
    long long int t;
    cin>>t;
    long long int i;
    for(i=0;i<t;i++)
    {
        long long int n;
        cin>>n;
        long long int u,v1;
        vector<int> v,key;
        int j=0;
        map<long long int,long long int> path;
        for(j=0;j<n;j++)
        {
            cin>>v1;
            v.push_back(v1);
        }
        for(j=0;j<n-1;j++)
        {
            cin>>u>>v1;
            key.push_back(u-1);
            key.push_back(v1-1);
            path[u-1]=v1-1;
        }
        int counter=0;
        for(j=0;j<n-1;j+=2)
        {
            if(v[key[j]]==0 && v[key[j+1]]==0)
            {
                int value=path[key[j+1]];
                if(v[value]==1)
                {
                    v[key[j]]=1;
                    counter++;
                }
                else
                {
                    v[key[j+1]]=1;
                    counter++;
                }
            }
            else if(v[key[j]]==1 && v[key[j+1]]==0)
            {
                int value=path[key[j+1]];
                if(v[value]==0)
                {
                    v[value]=1;
                    counter++;
                }
            }
        }
        cout<<counter<<endl;

    }
    return 0;
}
