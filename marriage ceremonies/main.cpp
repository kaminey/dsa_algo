#include<iostream>
using namespace std;
int index[16][16];
int function(int i,int j,int n)
{
    if(i>=0 && i<n && j>=0 && j<n)
    {
        int max1=index[i][j]+function(i+1,j-1,n);
        int max2=index[i][j]+function(i+1,j+1,n);
        int max3=max(max1,max2);
        int max4=function(i,j+1,n);
        max4=max(max3,max4);
        int max5=function(i+1,j,n);
        max1=max(max5,max4);
        return max1;

    }
    else
    {
        return 0;
    }

}
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        int n;
        cin>>n;
        int i,j;
        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
            {
                cin>>index[i][j];
            }
        }
        int maximum=function(0,0,n);
        cout<<maximum<<endl;
    }
}
