#include<iostream>
using namespace std;
#include<stack>
#include<string>
#include<map>
int main()
{
	stack<char> s;
	map<char,int> d;
	d['+']=1;
	d['-']=2;
	d['*']=3;
	d['/']=4;
	d['^']=5;
	string str;
	string result("");
	cin>>str;
	int i=0;
	while(i<str.size())
	{
		if(str[i]=='+' ||str[i]=='-' ||str[i]=='*' ||str[i]=='/' || str[i]=='^')
		{
			if(s.empty())
			{
				s.push(str[i]);
			}
			else
			{
				char c=s.top();
				if(d[str[i]]>=d[c])
				{
					s.push(str[i]);
				}
				while(d[str[i]]<d[c])
				{
					result=result+c;
					s.pop();
					if(s.empty())
					{
						s.push(str[i]);
						break;
					}
					else
					{
						c=s.top();
					}
				}
			}
		}
		else if(str[i]=='(')
		{
			s.push(str[i]);
		}
		else if(str[i]==')')
		{
			char c=s.top();
			while(c!=')')
			{
				if(c!='(')
				{
					result=result+c;
				}
				s.pop();
				if(!s.empty())
				{
					c=s.top();
				}
				else
				{
					break;
				}
			}
		}
		else
		{
			result=result+str[i];
		}
		i+=1;
	}
	while(!s.empty())
	{
		char c=s.top();
		result=result+c;
		s.pop();
	}
	cout.flush();
	cout<<result;
	return 0;
			
}
