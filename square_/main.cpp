#include<iostream>
using namespace std;
int main()
{
    int t;
    cin>>t;
    int i;
    for(i=0;i<t;i++)
    {
        long long int n;
        cin>>n;
        long long int sum,sum_of_squares;
        sum=(n*(n+1))/2;
        sum_of_squares=(n*(n+1)*(2*n+1))/6;
        long long int result=sum*sum-sum_of_squares;
        cout<<result<<endl;
    }
    return 0;
}
