#include<iostream>
using namespace std;
#include<vector>
#include<map>
void insert(map< long long int,vector< long long int > > &tree,long long int num,long long int i,long long int &counter)
{
    counter++;
    if(i==0 && counter==0)
    {
        tree[0].push_back(num);
    }
    else if(tree[i][0]<num)
    {
        if(tree[2*i+1].empty())
        {
            tree[2*i+1].push_back(num);
        }
        else
        {
            insert(tree,num,2*i+1,counter);
        }
    }
    else
    {
        if(tree[2*i+2].empty())
        {
            tree[2*i+2].push_back(num);
        }
        else
        {
            insert(tree,num,2*i+2,counter);
        }
    }

}
int main()
{
    long long int n;
    cin>>n;
    long long int i,counter;
    counter=-1;
    map< long long int,vector< long long  int > > tree;
    for(i=0;i<n;i++)
    {
        long long int num;
        cin>>num;
        insert(tree,num,0,counter);
        cout<<counter<<endl;
    }
    return 0;
}
