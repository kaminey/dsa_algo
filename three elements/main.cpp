#include<iostream>
using namespace std;
#include<vector>
#include<algorithm>
int main()
{
    int i,t;
    int n;
    cin>>t;
    for(i=0;i<t;i++)
    {
        cin>>n;
        vector<int> v;
        int j;
        for(j=0;j<n;j++)
        {
            int num;
            cin>>num;
            v.push_back(num);
        }
        sort(v.begin(),v.end());
        int sum=v[n-1]+v[n-2]+v[n-3];
        cout<<"Case"<<" "<<i+1<<":"<<" "<<sum<<endl;
    }
    return 0;
}
